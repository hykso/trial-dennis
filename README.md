# FightCamp Studio Web Application Trial

To maintain the service to the current customer, FightCamp needs to film and release around 16 to 20 workouts every month. Those workouts are usually filmed in FightCamp Studio in Costa Mesa in batches of 4 to 6 in a single day and require around 10 participants for each class. 

The participants usually get notified that a new schedule is ready and then can go on https://studio.joinfightcamp.com to book their spot for the desired workout. They select the workout and enter their information to complete the reservation. The Community Manager makes sure there is enough participants for each workout, but also not too many. That's why a workout can be grayed-out for the list.

The goal of this trial is to recreate the Schedules and Reservation components using a Frontend Framework (Vue, React, Angular, ...), Node/Express and Airtable as a "Database/CMS".

## Architecture

There are 3 main parts to this project: 

**Frontend**
  
   1. Fetch the upcoming `Schedules` with their associated `Workouts` from the backend server and populate the workout components.

   2. When a user press the 'Reserve', a form appear to gather the user's information and send it to the backend server to reserve a spot. 

**Node/Express Backend**

   1. Retrieves the workouts available from Airtable 

   2. Create new reservations in Airtable. 

**Airtable as a "SaaS database"**

   1. The Community Manager manages the upcoming `Schedules` and the `Workouts` in Airtable 

   2. The Community Manager can see the reservations for each workout.
   
   3. If a workout seems to have enough reservations, the Community manager can "close" the workout manually preventing any more reservations for that `Workout`. 

![MVIMG_20190801_175259](/.readme/MVIMG_20190801_175259.jpg)

## 1. Frontend

The frontend is separated in 3 views. 

1. Schedules
2. Reservation Form 
3. Confirmation

### Schedules

The Schedules view displays a list of workouts grouped by their associated Schedule. The user can navigate from one `Schedules` to another, but only one `Schedule` is shown at a time. 

![Screen Shot 2019-08-01 at 4.05.28 PM](/.readme/image1.png)

Here are more details on what the workout cell looks like. The information presented here is:
- Time (1:15pm, 2.35pm ...)
- Duration in minutes (30 minutes, 40 minutes, 50minutes)
- Difficulty of the workout (Open, Intermediate)
- Number of Rounds
- Name of the trainer
- Picture of the trainer

![Screen Shot 2019-08-01 at 3.59.11 PM](/.readme/image2.png)

Here's what the workout cell looks like when canceled.

![Screen Shot 2019-08-01 at 3.59.11 PM](/.readme/image5.png)

### Reservation Form

Once the user has selected a `Workout`, a form is shown with the workout's information and fields to enter the following information:

- Full Name
- Email
- Phone Number

The user can only reserve a spot when all the fields are filled. Once the user reserves his spot, the information should be sent to the backend. The user can go back to the schedules at any time, unless the reservation is being processed.

![Screen Shot 2019-08-01 at 3.59.44 PM](/.readme/image3.png)

### Confirmation

When the reservation has been processed successfully, a small message is displayed about the reserved workout

![Screen Shot 2019-08-01 at 4.02.58 PM](/.readme/image4.png)

## 2. Backend 

2 endpoints are required and use Airtable.

1.  `GET /schedules` 
2.  `POST /reservations` 

### `GET /schedules`

Retrieves the available `Schedules` with the associated `Workouts` from the Airtable table. The endpoint should only send back the `Schedules` where `Public` is set. 

### `POST /reservations`

Create a new `Reservation` with the user information for the selected workout's information.

## Other Directives

- No CSS framework 
- Vue.js and Node.js are preferred
- The UI in this document are only for reference. Creativity is welcome!

## What will be evaluated 

- Use of Vue and Express features for the right scenarios
- Responsiveness and Aesthetics of the UIs
- Code Readability and consistency
- Proper segmentation of the codes

## Quick tips 
- Don't be affraid to ask questions if you are not sure with certains aspects of the trial. 
- Make sure to breakdown the projects into smaller parts. It's okay if the project is not completed at the end of the trial, we don't expect it to be with the short timeframe, but being able to demo working features will help during the evaluation.


## Getting Started

This repository has 3 folders:
- `assets` -> A few files that can be useful for the project
- `frontend` -> Starter for using Vue CLI. `npm run serve` will start a local development server. If another framework is used, you can delete this folder and start over. 
- `backend` -> Starter for Express listening port 3000. `npm run serve` will start a local development server.

Here's an invitation to a prepopulated Airtable. https://airtable.com/invite/l?inviteId=inv5SA2FbCuV4PCpA&inviteToken=3918bfbee50fe26f99d91d5fe342fb8d01e43463047e3349ff29fba942071a9a

## Project submission

Use Git to commit your code during your trial and push your solution to the this repository.